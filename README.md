Knockout Bindings
=================

This is my collection of Knockout bindings that make my life easier.  I hope it
will make yours easier as well.


fit
---
The "fit" binding sizes an element to the remaining horizontal or vertical 
space.  You can use fit to easily create a viewport-sized application.

sizer
-----
The "sizer" converts an element into a draggable sizer handled for a neighboring element.

Note: the _fit_ and _sizer_ bindings combined can replace most layout libraries.
