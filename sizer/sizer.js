/*
* @name Knockout sizer binding
* @author Timothy Farrell
* @email explorigin@gmail.com
* @website https://bitbucket.org/explorigin/knockout-bindings
* 
* Based on the jQuery.splitter.js by Kristaps Kukurs (contact@krikus.com)
*/

(function(ko) {
    var side_defaults = {
            r:{ // Right side
                order:"prev",
                moving:"left",
                moving_opposite:"right",
                sizing: "width",
                eventPos: "pageX",
                cls: "sizer-right"
            },
            b:{ // Bottom side
                order:"prev",
                moving:"top",
                moving_opposite:"bottom",
                sizing: "height",
                eventPos: "pageY",
                cls: "sizer-bottom"
            }
            // TODO - support more sides
        },
        orientationDefaults = {
            min_size:0, //minimum width/height in PX.
            max_size:0, //maximum width/height in PX.
            ghost_class: 'sizer-ghost'// class name for ghosted sizer
        };

    function disableSelection(target){
        if (typeof target.onselectstart != "undefined") {//IE route
            target.onselectstart = function() { return false };
        } else if (typeof target.style.MozUserSelect != "undefined") { //Firefox route
            target.style.MozUserSelect = "none";
        } else {
            target.style['-webkit-user-select'] = "none";
        }
    }

    function enableSelection(target){
        target.onselectstart = undefined;
        try {
            target.style.MozUserSelect = null;
        } catch (e) {}
        try {
            target.style['-webkit-user-select'] = null;
        } catch (e) {}
        
    }

    function apply_move(e) {
        var incr = e[this.side.eventPos] - this.ghost_el.data("sizer_position"),
            sizer_init_pos = this.sizer_el.data("sizer_position");

        this.ghost_el.css(this.side.moving, sizer_init_pos[this.side.moving] + incr);
    }

    function end_drag(e) {
        var p = this.ghost_el.position(),
            percent;

        $(document).unbind("mousemove", this.apply_move)
                   .unbind("mouseup", this.end_drag);

        enableSelection(document);

        if (this.mode === 'percent') {
            percent = p[this.side.moving] - this.container.offset()[this.side.moving];
            this.current_position(percent / this.space_available() * 100);
        } else {
            this.current_position(p[this.side.moving]);
        }

        this.ghost_el.remove();
        this.ghost_el = null;  

        $(window).trigger("resize");
    }

    function start_drag(e) {
        var sizer_init_pos = this.sizer_el.position();

        if (this.ghost_el === null) {
            this.ghost_el = this.sizer_el.clone(false).insertAfter(this.slave_el);
        }

        sizer_init_pos[this.side.moving] -= get_element_size(this.sizer_el, this.side);
        this.sizer_el.data("sizer_position", sizer_init_pos);
        
        this.ghost_el.addClass(this.ghost_class)
            .width(this.sizer_el.width())
            .height(this.sizer_el.height())
            .css(this.side.moving, sizer_init_pos[this.side.moving]);
            
        this.ghost_el.data("sizer_position", e[this.side.eventPos]);

        disableSelection(document);

        $(document).bind("mousemove", this.apply_move)
                   .bind("mouseup", this.end_drag);
    }

    function get_element_size(el, side) {
        var size;

        size = el[side.sizing]();
        size += parseInt(el.css('border-' + side.moving + '-width'), 10);
        size += parseInt(el.css('border-' + side.moving_opposite + '-width'), 10);
        return size;
    }

    ko.bindingHandlers.sizer = {
        init: function(element, valueAccessor) {
            var sizer_el = $(element),
                ctx, side, size, percent, settings, mode;

            settings = ko.utils.unwrapObservable(valueAccessor());
            if (typeof(settings) === 'string') {
                settings = {
                    'side': settings,
                    'size': ko.observable()
                };
            }

            side = side_defaults[settings.side];
            size = ko.utils.unwrapObservable(settings.size);
            if (typeof(size) === 'string' && size.indexOf('%') !== -1) {
                mode = 'percent';
                if (ko.isObservable(settings.size)) {
                    // FIXME - if the observable has a percent sign, don't remove it.
                    settings.size(parseInt(size, 10));
                } else {
                    settings.size = parseInt(size, 10);
                }
            } else {
                mode = 'fixed';
            }

            ctx = $.extend(
                {
                    mode: mode,
                    side: side_defaults[settings.side],
                    sizer_el: sizer_el,
                    slave_el: sizer_el[side.order](),
                    ghost_el: null,
                    container: sizer_el.parent(),
                    space_available: ko.observable(sizer_el.parent()[side.sizing]()),
                    current_position: (ko.isObservable(settings.size)) ?
                        settings.size : ko.observable(settings.size)
                },
                orientationDefaults);


            // Apply the correct class
            ctx.sizer_el.addClass(side.cls);
            ctx.sizer_el.addClass('sizer-' + mode);

            if (mode === 'percent') {
                ctx.space_available.subscribe(function() {
                    this.current_position.notifySubscribers(this.current_position());
                }.bind(ctx));

                $(window).resize(function(e, size) {
                    this.space_available(this.container[this.side.sizing]());
                }.bind(ctx));
            }

            ctx.current_position.subscribe(function (val) {
                if (ctx.mode === 'percent') {
                    this.slave_el.show().css(this.side.sizing,
                                             Math.max(0, val) + '%');                
                } else {
                    this.slave_el.show().css(this.side.sizing, val + 'px');
                }
            }.bind(ctx));

            // Set our initial position
            if (!ctx.current_position()) {
                if (mode === 'percent') {
                    percent = ctx.sizer_el.position()[side.moving] - ctx.container.offset()[side.moving];
                    ctx.current_position(percent / ctx.space_available() * 100);
                } else {
                    ctx.current_position();
                }
            }
            ctx.current_position.notifySubscribers(ctx.current_position());

            // Here we go through the looking glass (binding methods to their container)
            ctx.apply_move = apply_move.bind(ctx);
            ctx.end_drag = end_drag.bind(ctx);

            sizer_el.mousedown(start_drag.bind(ctx));
        }
    }
})(ko);