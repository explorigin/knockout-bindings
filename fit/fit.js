/*
* @name Knockout fit binding
* @author Timothy Farrell
* @email explorigin@gmail.com
* @website https://bitbucket.org/explorigin/knockout-bindings
* 
* fit will size the bound element to fill the available space. Receives one param:
* - "v" for vertically
* - "h" for horizontally
*/

(function(ko) {
    var orientationSettings = {
        h:{
            moving:"left",
            moving_opposite:"right",
            sizing: "width"
        },
        v:{
            moving:"top",
            moving_opposite:"bottom",
            sizing: "height"
        }
    };

    function get_sibling_dimension(el, opts) {
        var i, sib, sibs = el.siblings(), dimension = 0;

        for (i=0; i < sibs.length; ++i) {
            sib = $(sibs[i]);
            if (sib.is(":visible")) {
                dimension += sib[opts.sizing]()
                dimension += parseInt(sib.css('margin-' + opts.moving), 10)
                dimension += parseInt(sib.css('margin-' + opts.moving_opposite), 10)
                dimension += parseInt(sib.css('padding-' + opts.moving), 10);
                dimension += parseInt(sib.css('padding-' + opts.moving_opposite), 10);
            }
        }

        return dimension
    }

    function recalculate(el, orientation) {
        var par = el.parent(),
            opts = orientationSettings[orientation],
            available = par[opts.sizing]();

        if (el.siblings().length === 0) {
            available -= parseInt(par.css('padding-' + opts.moving), 10);
            available -= parseInt(par.css('padding-' + opts.moving_opposite), 10);
        }

        el[opts.sizing](available - get_sibling_dimension(el, opts));
    }

    ko.bindingHandlers.fit = {
        init: function(element, valueAccessor) {
            var settings = valueAccessor(),
                el = $(element),
                orientation = ko.utils.unwrapObservable(settings.orientation || "v"),
                enabled = ko.utils.unwrapObservable(settings.enabled || true);

            function check_recalc() {
                if (enabled) {
                    recalculate(el, orientation)
                }
            }

            $(window).resize(check_recalc);
        },
        update: function(element, valueAccessor) {
            var settings = valueAccessor(),
                el = $(element),
                orientation = ko.utils.unwrapObservable(settings.orientation || "v"),
                enabled = ko.utils.unwrapObservable(settings.enabled || true);

            if (enabled) {
                recalculate(el, orientation)
            }
        }
    };
})(ko);